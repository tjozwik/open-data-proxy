require 'aws-sdk-dynamodb'

class DynamoDB
    def initialize(table_name)
        @dynamodb = Aws::DynamoDB::Client.new(
            region: ENV['DYNAMODB_REGION'],
            access_key_id: ENV['DYNAMODB_ACCESS_KEY'],
            secret_access_key: ENV['DYNAMODB_SECRET'],
            endpoint: ENV['DYNAMODB_ENDPOINT']
        )
        @table_name = table_name
    end

    def push(item)
        if item['dataset'].empty?
            item['dataset'] = '-'
        end
        @dynamodb.put_item({
            item: item,
            table_name: @table_name
        })
    end
    
    def scan
        @dynamodb.scan({
            table_name: @table_name
        })
    end

    def fetch(item)
        @dynamodb.get_item({
            table_name: @table_name,
            key: item
        })
    end

    def update(item)
        key = {
            'ip_log' => "#{item['ip']}_#{item['dataset']}_#{Date.today.to_s}}"
        }
        log = fetch(key)
        if log['item'].nil?
            push(item.merge({
                'count' => '1',
                'last_access' => Date.today.to_s
        }.merge(key)))
        else
            log['item']['count'] = log['item']['count'].to_i + 1
            log['item']['last_access'] = Date.today.to_s
            push(log['item'])
        end
    end

    def drop(item)
        @dynamodb.delete_item({
            table_name: @table_name,
            key: item
        })
    end
end