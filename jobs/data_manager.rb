require_relative '../models/record_model'

log_database = DynamoDB.new(ENV['LOGGING_DATABASE'])
stats_database = DynamoDB.new(ENV['STATS_DATABASE'])
log_database.scan.items.each do |item|
    stats_database.update(item)
    log_database.drop({
        request: item['request']
    })
end