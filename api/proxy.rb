require_relative '../models/record_model'
require 'httparty'
require 'sinatra'
require 'sinatra/namespace'

before do
    content_type :json
end

get '/data-stats' do
    stats = DynamoDB.new(ENV['STATS_DATABASE']).scan.items
    stats.each do |s|
        s.delete('request')
        s.delete('ip_log')
        s['dataset'] = s['dataset'][9..-1]
        s['latitude'] = s['latitude'].to_f.to_s
        s['longitude'] = s['longitude'].to_f.to_s
        s['count'] = s['count'].to_i.to_s
        s['internet_provider'] = s.delete('org')
    end
    stats.to_json
end

get '/*' do    
    api_request = HTTParty.get("https://api.dane.gov.pl/#{params['splat'].first}")
    api_request_info = HTTParty.get("https://ipapi.co/#{request.ip}/json/").merge({
        'request' => "#{request.ip}_#{DateTime.now.to_s}_#{Random.new_seed}",
        'dataset' => params['splat'].first,
        'user_agent' => request.user_agent
    })
    DynamoDB.new(ENV['LOGGING_DATABASE']).push(api_request_info)
    api_request.body
end